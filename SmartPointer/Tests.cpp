#include "testslib.hpp"
#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS
#include "SmartPointer.hpp"
#include "WeakPointer.hpp"

namespace AZ::Tests
{
DECLARE_OOP_TEST(test_simple_ptr)
{
	SmartPointer<int> a(new int (5));
	assert(*a.get() == 5);
	assert(a.getUsesCount() == 1);
	SmartPointer<int> b = a;
	assert(a.getUsesCount() == 2);
	assert(b.getUsesCount() == 2);
	assert(*b == 5);
	a.reset();
	assert(*b == 5);
	assert(b.getUsesCount() == 1);
}

DECLARE_OOP_TEST(test_some_funcs)
{
	std::unique_ptr<int> p(new int(4));
	SmartPointer<int> a(std::move(p));
	assert(*a == 4);
	assert(a.getUsesCount() == 1);
	assert(!p);

	SmartPointer<int> p1(new int(3));
	SmartPointer<int> p2 = std::move(p1);
	assert(!p1);
	assert(p2.getUsesCount() == 1);

	SmartPointer<int> p3(new int(3));
	SmartPointer<int> p4(std::move(p1));
	assert(!p1);
	assert(p2.getUsesCount() == 1);

	double *param = new double(10);
	SmartPointer<double> p5(param);
	auto p6 = p5;
	auto p7 = p6;
	auto p8 = p7;

	assert(*p5 == *param);

	assert(p5.getUsesCount() == 4);
	assert(p6.getUsesCount() == 4);
	assert(p7.getUsesCount() == 4);
	assert(p8.getUsesCount() == 4);
	SmartPointer<double> p9 = std::move(p5);
	assert(p6.getUsesCount() == 4);
	assert(p7.getUsesCount() == 4);
	assert(p8.getUsesCount() == 4);
	assert(p9.getUsesCount() == 4);
	p6.reset();
	assert(!p6);
	assert(p7.getUsesCount() == 3);
	assert(p8.getUsesCount() == 3);
	assert(p9.getUsesCount() == 3);
	p7.reset();
	p8.reset();
	assert(param);
	assert(p9.getUsesCount() == 1);
	p9.reset();
	assert(p9.getUsesCount() == 0);

}

DECLARE_OOP_TEST(test_make_smart)
{
	auto p = makeSmartPointer<std::string>("kek");
	assert(*p == "kek");
	auto p1 = p;
	assert(*p1 == *p1);
	assert(p1 == p);
	assert(p1.getUsesCount() == 2);

	SmartPointer<int> p2 = nullptr;
	assert(p2.getUsesCount() == 0);

	auto a = makeSmartPointer<int>(3);
	auto b = a;
	auto c = a;
	a.reset(new int(5));
	assert(b.getUsesCount() == 2);
	assert(c.getUsesCount() == 2);
	assert(*b == 3);
	assert(*c == 3);
	assert(a.getUsesCount() == 1);
	assert(*a == 5);

	auto a1 = makeSmartPointer<std::string>("one");
	auto b1 = a1;
	auto c1 = a1;
	a1.reset(new std::string);
	*a1 = "two";
	assert(b1.getUsesCount() == 2);
	assert(c1.getUsesCount() == 2);
	assert(*b1 == "one");
	assert(*c1 == "one");
	assert(a1.getUsesCount() == 1);
	assert(*a1 == "two");
	a1.reset();
	assert(!a1);
}

DECLARE_OOP_TEST(test_swap)
{
	auto a = makeSmartPointer<int>(10);
	auto b = makeSmartPointer<int>(20);
	auto c = a;

	assert(*c == 10);
	assert(c.getUsesCount() == 2);
	assert(a.getUsesCount() == 2);
	assert(b.unique());

	a.swap(b);
	assert(*a == 20);
	assert(*b == 10);

	assert(b.getUsesCount() == 2);
	assert(a.unique());
	assert(c.getUsesCount() == 2);

}

DECLARE_OOP_TEST(test_copy_assigment)
{
	SmartPointer<int> a(new int(1));
	SmartPointer<int> a1(a);
	SmartPointer<int> a2(a1);
	assert(a.getUsesCount() == 3);
	assert(a1.getUsesCount() == 3);
	assert(a2.getUsesCount() == 3);


	auto b = makeSmartPointer<int>(2);
	auto b1 = b;

	assert(b.getUsesCount() == 2);
	assert(b1.getUsesCount() == 2);

	b1 = a;
	assert(b.getUsesCount() == 1);
	assert(a.getUsesCount() == 4);
	assert(b1.getUsesCount() == 4);
	assert(*b1 == 1);
	assert(*b.get() == 2);

	b = a2;
	assert(b.getUsesCount() == 5);
	assert(b == a);
	assert(*b == 1);

	a.reset(new int(3));
	assert(b.getUsesCount() == 4);
	assert(b1.getUsesCount() == 4);
	assert(a1.getUsesCount() == 4);
	assert(*a == 3);
}

DECLARE_OOP_TEST(test_move_assigment)
{
	SmartPointer<int> p1(new int(3));
	SmartPointer<int> p2(p1);
	SmartPointer<int> p3 = std::move(p1);
	assert(!p1);
	assert(p2.getUsesCount() == 2);
	p1.reset(new int(5));
}

DECLARE_OOP_TEST(test_custom_deleter)
{
	struct Del
	{
		void operator()(int *_p)
		{
			::operator delete (_p);
		}
	};
	int * val = new int(3);
	SmartPointer<int> p1(std::move(val), Del{});
	assert(p1);
	assert(p1.getUsesCount() == 1);
	assert(*p1 == 3);
	assert(p1.unique());

	{
		struct Del1
		{
			void operator () (std::string *_p)
			{
				delete _p;
			}
		};
		SmartPointer<std::string> p2(new std::string("kek"), Del1{});
		SmartPointer<std::string> p3 = p2;
		assert(p2.getUsesCount() == 2);
		assert(*p2 == *p3);
		p2.reset();
	}

	{
		struct Del2
		{
			void operator () (double  *_p)
			{
				delete _p;
			}
		};
		SmartPointer<double> p4(new double(10.0), Del2{});
		SmartPointer<double> p5 = std::move(p4);
		assert(p5.getUsesCount() == 1);
		assert(!p4);
	}

	{
		struct Del3
		{
			void operator () (int *_p)
			{
				delete _p;
			}
		};
		SmartPointer<int>p6(new int(3));
		SmartPointer<int> p7(new int(5), Del3{});
		p7 = p6;
		assert(p7.getUsesCount() == 2);
		assert(p6.getUsesCount() == 2);
		assert(*p6 == *p7);
	}

}

DECLARE_OOP_TEST(test_array)
{
	SmartPointer<int[]> p1(new int[10]);
	for (int i{}; i < 10; i++)
		p1[i] = i;

	for (int i{}; i < 10; i++)
		assert(p1[i] == i);
	assert(p1);
	assert(p1.getUsesCount() == 1);
}

DECLARE_OOP_TEST(test_weak_ptr)
{
	SmartPointer<int> p1(new int(1));
	SmartPointer<int> p2 = p1;

	assert(p1.getUsesCount() == 2);
	assert(p2.getUsesCount() == 2);

	WeakPointer<int> w1 = p1;
	assert(w1.getUsesCount() == 2);
	assert(p1.getUsesCount() == 2);
	assert(p2.getUsesCount() == 2);

	auto p4 = w1.lock();
	assert(p1.getUsesCount() == 3);
	assert(p2.getUsesCount() == 3);
	assert(p4.getUsesCount() == 3);
	assert(w1.getUsesCount() == 3);
	
	assert(*p1 == *p2);
	assert(*p4 == 1);

	w1.reset();
	assert(*p1 == *p2);
	assert(*p4 == 1);
	assert(w1.getUsesCount() == 0);

	p2.reset();
	p4.reset();
	std::shared_ptr<int> a;
}

//DECLARE_OOP_TEST(test_allocator)
//{
//	struct Del
//	{
//		void operator () (int *_p)
//		{
//			delete _p;
//		}
//	};
//	SmartPointer<int> p1(new int(3), Del{}, std::allocator<int>{});
//	assert(p1);
//	assert(*p1 == 3);
//	assert(p1.getUsesCount() == 1);
//}

}