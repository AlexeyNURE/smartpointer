#ifndef _SMARTPOINTER_HPP_
#define _SMARTPOINTER_HPP_
#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS


#include <iostream>
#include <memory>
#include <type_traits>
#include <utility>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <queue>
#include <list>
#include <unordered_set>
#include <unordered_map>
#include <forward_list>
#include <deque>
#include "RefCount.hpp"

namespace AZ
{

template< typename _Pointer >
class WeakPointer;

template< typename _Pointer >
class SmartPointer
{
private:
	friend class WeakPointer<_Pointer>;

	using value_type = _Pointer;
	using pure_type = typename RemoveArray<_Pointer>::Type;
	using pointer = pure_type *;
	using reference = pure_type & ;
	using const_reference = const pure_type &;

	static const int isArray{ std::is_array<value_type>::value };

	pointer m_pointer;
	RefCount<pure_type> * m_count;
	void copy(const SmartPointer &);
	void move(SmartPointer &&);
public:

	/*****************CONSTRUCTORS********************/

	constexpr SmartPointer();  // will be built in compile time

	constexpr SmartPointer(nullptr_t);

	explicit SmartPointer(pointer);

	SmartPointer( const AZ::WeakPointer<_Pointer> & _weak_ptr); // object from weak_ptr

	template<typename Deleter>
	explicit SmartPointer(pointer, Deleter  _deleter);

	template <typename Deleter, typename Alloc>
	explicit SmartPointer(pure_type *, Deleter _d, Alloc _all);

	template<typename Deleter>
	explicit SmartPointer(Deleter _d);  // with nullptr

	template <typename Deleter, typename Alloc>
	explicit SmartPointer(Deleter _d, Alloc _all); // with nullptr

	template <typename = typename std::enable_if< isArray>::type>
	explicit SmartPointer(pointer _ptr);

	~SmartPointer();

	SmartPointer(const SmartPointer &);

	SmartPointer operator = (const SmartPointer &);

	SmartPointer(SmartPointer &&);

	SmartPointer operator = (SmartPointer &&);

	SmartPointer(std::unique_ptr<_Pointer> &&);

	/******************READ METHODS********************/

	pointer get() const noexcept;

	reference operator *() const noexcept;

	pointer operator ->() const noexcept;

	int getUsesCount() const noexcept;

	bool unique() const noexcept;

	template <typename = typename std::enable_if<isArray>::type>
	reference operator [] (int _index) noexcept;

	template <typename = typename std::enable_if<isArray>::type>
	const_reference operator [] (int _index) const noexcept;

	/**************MODIFICATIOM METHODS****************/

	void reset();

	void reset(pointer);

	void swap(SmartPointer &) noexcept;

	/**********************OTHER*************************/

	bool operator == (const SmartPointer<_Pointer> &) const noexcept;

	bool operator != (const SmartPointer<_Pointer> &) const noexcept;

	operator bool() const noexcept;

	template <class T, class U, class V>
	friend std::basic_ostream<U, V>& operator<<(std::basic_ostream<U, V>& os, const SmartPointer<V> &_ptr) noexcept;
	
};


template<typename T, class ...Args>
inline SmartPointer<T> makeSmartPointer(Args && ...args) // universal reference
{
	return SmartPointer<T>(new T(std::forward<Args>(args)...));
}

template <class T, class U, class V>
std::basic_ostream<U, V>& operator<<(std::basic_ostream<U, V>& os, const SmartPointer<V>& _ptr) noexcept
{
	os << *_ptr;
	return os;
}

template<typename _Pointer>
template <typename = typename std::enable_if<isArray>::type>
inline typename AZ::SmartPointer<_Pointer>::reference AZ::SmartPointer<_Pointer>::operator[](int _index) noexcept
{
	return get()[_index];
}

template<typename _Pointer>
template <typename = typename std::enable_if<isArray>::type>
inline typename AZ::SmartPointer<_Pointer>::const_reference AZ::SmartPointer<_Pointer>::operator[](int _index) const noexcept
{
	return get()[_index];
}

template<typename _Pointer>
template<typename Deleter>
AZ::SmartPointer<_Pointer>::SmartPointer(pointer _ptr, Deleter _d)
	:m_pointer(std::move(_ptr))
{
	m_count = new RefCountWithManagement<pure_type, Deleter>(_ptr);
	m_count->incrementRefCount();
}

template<typename _Pointer>
template<typename Deleter>
AZ::SmartPointer<_Pointer>::SmartPointer(Deleter _d)
{
	m_pointer = nullptr;
	m_count = new RefCountWithManagement<pure_type, Deleter>(nullptr);
	m_count->incrementRefCount();
}

//TODO: make custom allocator work
template<typename _Pointer>
template<typename Deleter, typename Alloc>
inline AZ::SmartPointer<_Pointer>::SmartPointer(pure_type *_ptr, Deleter _d, Alloc _all)	
	:m_pointer(_ptr)
{
	_all.allocate(sizeof(RefCountWithManagement<pure_type, Deleter, Alloc>), m_count);
	new (m_count) RefCountWithManagement<pure_type, Deleter, Alloc>(_ptr);
}


template<typename _Pointer>
inline constexpr AZ::SmartPointer<_Pointer>::SmartPointer()
	: m_pointer(nullptr), m_count(nullptr)      // construct empty smart_ptr
{
}

}


template class AZ::SmartPointer<bool>;
template class AZ::SmartPointer<char>;
template class AZ::SmartPointer<signed char>;
template class AZ::SmartPointer<unsigned char>;
template class AZ::SmartPointer<char16_t>;
template class AZ::SmartPointer<char32_t>;
template class AZ::SmartPointer<wchar_t>;
template class AZ::SmartPointer<short>;
template class AZ::SmartPointer<unsigned short>;
template class AZ::SmartPointer<int>;
template class AZ::SmartPointer<int[]>;
template class AZ::SmartPointer<double[]>;
template class AZ::SmartPointer<unsigned int>;
template class AZ::SmartPointer<long>;
template class AZ::SmartPointer<long long>;
template class AZ::SmartPointer<unsigned long>;
template class AZ::SmartPointer<unsigned long long>;
template class AZ::SmartPointer<float>;
template class AZ::SmartPointer<double>;
template class AZ::SmartPointer<long double>;
template class AZ::SmartPointer<std::string>;
template <typename T> class AZ::SmartPointer<std::vector<T>>;
template <typename T> class AZ::SmartPointer<std::deque<T>>;
template <typename T> class AZ::SmartPointer<std::queue<T>>;
template <typename T> class AZ::SmartPointer<std::priority_queue<T>>;
template <typename T> class AZ::SmartPointer<std::list<T>>;
template <typename T> class AZ::SmartPointer<std::forward_list<T>>;
template <typename T> class AZ::SmartPointer<std::set<T>>;
template <typename T> class AZ::SmartPointer<std::unordered_set<T>>;
template <typename T, typename U> class AZ::SmartPointer<std::map<T, U>>;
template <typename T, typename U> class AZ::SmartPointer<std::unordered_map<T, U>>;

#endif