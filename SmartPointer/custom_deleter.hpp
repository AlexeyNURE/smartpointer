#ifndef _CUSTOM_DELETER_HPP
#define _CUSTOM_DELETER_HPP
#include <iostream>

template <typename T>
class CustomDeleter
{
	using value_type = T;
	using pointer = value_type * ;
	using reference = value_type & ;
	using const_reference = const value_type &;

	CustomDeleter() = default;
	CustomDeleter(const CustomDeleter &) = default;

	template <typename U>
	CustomDeleter(const U &)
	{
	}

	void operator ()(pointer _p)
	{
		::operator delete(_p);
	}
};


#endif // !_CUSTOM_DELETER_HPP
