#ifndef _WEAKPOINTER_HPP
#define _WEAKPOINTER_HPP
namespace AZ
{

template <typename _Pointer>
class WeakPointer
{
	friend SmartPointer<_Pointer>;
	using value_type = _Pointer;
	using pure_type = typename RemoveArray<value_type>::Type;
	using pointer = pure_type * ;
	using reference = pure_type & ;
	using const_reference = const reference;

	RefCount<pure_type> * m_count;
public:

	constexpr WeakPointer() noexcept;
	WeakPointer(const WeakPointer &) noexcept;
	WeakPointer & operator = (const WeakPointer &) noexcept;
	WeakPointer(WeakPointer &&) noexcept;
	WeakPointer(const SmartPointer<_Pointer> &) noexcept;
	SmartPointer<_Pointer> lock() const; 
	bool expired() const;
	void reset() noexcept;
	int getUsesCount() const;
	operator bool() const noexcept;
};

template <typename _Pointer>
inline constexpr WeakPointer<_Pointer>::WeakPointer() noexcept
	: m_pointer(nullptr), m_count(nullptr)
{
}

template<typename _Pointer>
AZ::WeakPointer<_Pointer>::WeakPointer(const WeakPointer &_ptr) noexcept
{
	if (!_ptr)
		return;

	m_count = _ptr.m_count;
	m_count->incrementWeakCount();
	m_count->m_pointer = _ptr.m_count->m_pointer;
}

template<typename _Pointer>
WeakPointer<_Pointer> & AZ::WeakPointer<_Pointer>::operator=(const WeakPointer & _ptr) noexcept
{
	if (*this == _ptr)
		return *this;
	reset();
	m_count = _ptr.m_count;
	m_count->incrementWeakCount();
	m_count->m_pointer = _ptr.m_count->m_pointer;
	return *this;
}

template<typename _Pointer>
void AZ::WeakPointer<_Pointer>::reset() noexcept
{
	if (!*this)
		return;
	m_count->decrementWeakCount();
	if (m_count->getWeakCount() == 0)
	{
		m_count->m_pointer = nullptr;
		m_count = nullptr;
	}
}

template<typename _Pointer>
AZ::WeakPointer<_Pointer>::WeakPointer(const SmartPointer<_Pointer>& _smart_ptr) noexcept
{
	if (!_smart_ptr)
	{
		m_count->m_pointer = nullptr;
		m_count = nullptr;
		return;
	}
	m_count = _smart_ptr.m_count;
	m_count->m_pointer = _smart_ptr.m_pointer;
	m_count->incrementWeakCount();
}

template<typename _Pointer>
SmartPointer<_Pointer> AZ::WeakPointer<_Pointer>::lock() const
{
	return expired() ? SmartPointer<_Pointer>() : SmartPointer<_Pointer>(*this);
}

template<typename _Pointer>
inline int AZ::WeakPointer<_Pointer>::getUsesCount() const
{
	return m_count ? m_count->getCount() : 0;
}

template<typename _Pointer>
inline bool AZ::WeakPointer<_Pointer>::expired() const
{
	return this->getUsesCount() == 0;
}

template<typename _Pointer>
inline AZ::WeakPointer<_Pointer>::operator bool() const noexcept
{
	return m_count->m_pointer != nullptr;
}

}
#endif // !_WEAKPOINTER_HPP
