#ifndef CUSTOM_ALLOCATOR_HPP_
#define CUSTOM_ALLOCATOR_HPP_
#include <iostream>


template <typename T>
class CustomAllocator
{
	using value_type = T;
	using pointer = value_type * ;
	using reference = value_type & ;
	using const_reference = const value_type & ;


	CustomAllocator() = default;
	CustomAllocator(const CustomAllocator &) = default;

	template <typename U>
	CustomAllocator(const U &)
	{
	};
	pointer allocate(std::size_t _size)
	{
		return static_cast<pointer> (::operator new(_size * sizeof(T)));
	}
	void deallocate(pointer _p, std::size_t _size)
	{
		::operator delete (_p);
	}

};

#endif // !CUSTOM_ALLOCATOR_HPP_
