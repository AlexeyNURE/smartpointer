#include "SmartPointer.hpp"
#include "WeakPointer.hpp"

template<typename _Pointer>
AZ::SmartPointer<_Pointer>::SmartPointer(pointer _ptr)
	:m_pointer(std::move(_ptr))
{
	m_count = new RefCountWithManagement<pure_type>(_ptr);
	m_count->incrementRefCount();
}

template<typename _Pointer>
constexpr AZ::SmartPointer<_Pointer>::SmartPointer(nullptr_t)
	:SmartPointer()
{
}

template<typename _Pointer>
AZ::SmartPointer<_Pointer>::SmartPointer(const AZ::WeakPointer<_Pointer> &_weak_ptr)
	: m_count(_weak_ptr.m_count), m_pointer(_weak_ptr.m_count->m_pointer)
{
	m_count->incrementRefCount();
}

template<typename _Pointer>
void AZ::SmartPointer<_Pointer>::copy(const SmartPointer &_smart_ptr)
{
	if (!_smart_ptr)
		return;

	m_pointer = _smart_ptr.get();
	m_count = _smart_ptr.m_count;
	m_count->incrementRefCount();
}

template<typename _Pointer>
AZ::SmartPointer<_Pointer>::SmartPointer(const SmartPointer &_smart_ptr)
{
	if (*this == _smart_ptr)
		return;

	copy(_smart_ptr);
}

template<typename _Pointer>
AZ::SmartPointer<_Pointer> AZ::SmartPointer<_Pointer>::operator=(const SmartPointer &_smart_ptr)
{
	if (*this == _smart_ptr)
		return *this;

	reset();
	copy(_smart_ptr);
	return *this;
}

template<typename _Pointer>
void AZ::SmartPointer<_Pointer>::move(SmartPointer &&_ptr)
{
	m_pointer = std::move(_ptr.get());
	m_count = std::move(_ptr.m_count);
	_ptr.m_pointer = nullptr;
	_ptr.m_count = nullptr;
}

template<typename _Pointer>
AZ::SmartPointer<_Pointer>::SmartPointer(SmartPointer &&_ptr)
{
	move(std::move(_ptr));
}

template<typename _Pointer>
AZ::SmartPointer<_Pointer> AZ::SmartPointer<_Pointer>::operator=(SmartPointer &&_ptr)
{
	move(std::move(_ptr));
	return *this;
}

template<typename _Pointer>
AZ::SmartPointer<_Pointer>::SmartPointer(std::unique_ptr<_Pointer> &&_ptr)
	:SmartPointer()
{
	m_pointer = std::move(_ptr.get());
	m_count = new RefCountWithManagement<pure_type>(_ptr.get());
	_ptr.release();
	m_count->incrementRefCount();
}

template <typename _Pointer>
template <typename = typename std::enable_if< isArray >::type>
AZ::SmartPointer<_Pointer>::SmartPointer(pointer _ptr)
{
	m_pointer = _ptr;
	struct Del
	{
		void operator ()(pointer p)
		{
			delete[] p;
		}
	};
	m_count = new RefCountWithManagement<pure_type, Del>(_ptr);
	m_count->increment();
}

template<typename _Pointer>
AZ::SmartPointer<_Pointer>::~SmartPointer()
{
	if (!m_count)
		return;
	if (m_count->getCount() > 1)
		m_count->decrementRefCount();
	else if (m_count->getCount() == 1)
	{
		m_count->release(); // m_count->m_Deleter.release();
		delete m_count;
	}
}

template<typename _Pointer>
inline AZ::SmartPointer<_Pointer>::operator bool() const noexcept
{
	return m_pointer != nullptr;
}

template<typename _Pointer>
typename AZ::SmartPointer<_Pointer>::pointer AZ::SmartPointer<_Pointer>::get() const noexcept
{
	return m_pointer;
}

template<typename _Pointer>
void AZ::SmartPointer<_Pointer>::reset()
{
	if (!*this)
		return;
	m_count->decrementRefCount();
	if (m_count->getCount() == 0)
	{
		m_count->release();
		delete m_count;
	}
	m_pointer = nullptr;
	m_count = nullptr;
}

template<typename _Pointer>
void AZ::SmartPointer<_Pointer>::reset(pointer _ptr)
{
	reset();
	m_pointer = std::move(_ptr);
	m_count = new RefCountWithManagement<pure_type>(_ptr);
	m_count->incrementRefCount();
}

template<typename _Pointer>
int AZ::SmartPointer<_Pointer>::getUsesCount() const noexcept
{
	return m_count ? m_count->getCount() : 0;
}

template<typename _Pointer>
typename AZ::SmartPointer<_Pointer>::reference AZ::SmartPointer<_Pointer>::operator*() const noexcept
{
	return *m_pointer;
}

template<typename _Pointer>
bool AZ::SmartPointer<_Pointer>::operator==(const SmartPointer<_Pointer>& _ptr) const noexcept
{
	return m_pointer == _ptr.get();
}

template<typename _Pointer>
typename AZ::SmartPointer<_Pointer>::pointer AZ::SmartPointer<_Pointer>::operator->() const noexcept
{
	return m_pointer;
}

template<typename _Pointer>
bool AZ::SmartPointer<_Pointer>::operator!=(const SmartPointer<_Pointer>& _ptr) const noexcept
{
	return !(*this == _ptr);
}

template<typename _Pointer>
bool AZ::SmartPointer<_Pointer>::unique() const noexcept
{
	return getUsesCount() == 1;
}

template<typename _Pointer>
void AZ::SmartPointer<_Pointer>::swap(SmartPointer &_ptr) noexcept
{
	std::swap(m_pointer, _ptr.m_pointer);
	std::swap(m_count, _ptr.m_count);
}
