#ifndef _REFCOUNT_HPP_
#define _REFCOUNT_HPP_
#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS
#include <functional>



template <typename T>
struct RemoveArray
{
	using Type = T;
};

template <typename T>
struct RemoveArray<T []>
{
	using Type = T;
};


template <typename T>
struct RefCount
{
	int m_count;
	int m_weak_count;
	T * m_pointer;


	RefCount(T *_t)
	{
		m_count = 0;
		m_weak_count = 0;
		m_pointer = _t;
	}
	void incrementRefCount()
	{
		m_count++;
	}
	void decrementRefCount()
	{
		m_count--;
	}
	void incrementWeakCount()
	{
		m_weak_count++;
	}
	void decrementWeakCount()
	{
		m_weak_count--;
	}

	inline int getCount() const noexcept
	{
		return m_count;
	}

	inline int getWeakCount() const noexcept
	{
		return m_weak_count;
	}

	T * get()
	{
		return m_pointer;
	}

	virtual void allocate(T *) = 0;
	virtual void deallocate(T *) = 0;
	virtual void release() = 0;
};


template <typename T,typename _Deleter = std::default_delete<T>, typename _Alloc = std::allocator< T >>
struct RefCountWithManagement
	: public RefCount<T>
{
	_Alloc m_Allocator;
	_Deleter m_Deleter;

	RefCountWithManagement( T *_t)
		:RefCount<T>(_t)
	{

	}
	void allocate(T *_t) override
	{
		//m_Allocator.allocate(_t);
	}

	void deallocate(T *_t) override
	{
	//	m_Allocator.deallocate();
	}

	void release() override
	{
		m_Deleter(this->get());
	}

};


#endif